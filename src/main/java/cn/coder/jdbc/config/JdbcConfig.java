package cn.coder.jdbc.config;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

/**
 * jdbc的配置对象
 * @author YYDF
 *
 */
public class JdbcConfig implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1393426141804057656L;

	private int queryTimeout = 5;
	private boolean multiQueries = true;
	private Map<String, DataSource> dataSources;

	public int getQueryTimeout() {
		return queryTimeout;
	}

	public void setQueryTimeout(int queryTimeout) {
		this.queryTimeout = queryTimeout;
	}

	public boolean isMultiQueries() {
		return multiQueries;
	}

	public void setMultiQueries(boolean multiQueries) {
		this.multiQueries = multiQueries;
	}

	public DataSource getDataSource(String source) {
		if (this.dataSources == null)
			return null;
		return this.dataSources.get(source);
	}

	public Map<String, DataSource> getDataSources() {
		return this.dataSources;
	}

	public void addDataSource(String key, DataSource ds) {
		if (this.dataSources == null)
			this.dataSources = new HashMap<>();
		this.dataSources.put(key, ds);
	}

}
