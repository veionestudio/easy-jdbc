package cn.coder.jdbc.util;

public class Assert {

	public static void notNull(Object obj, String error) {
		if(obj == null)
			throw new NullPointerException(error);
	}

}
