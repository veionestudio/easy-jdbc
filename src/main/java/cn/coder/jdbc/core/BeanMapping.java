package cn.coder.jdbc.core;

import java.lang.reflect.Field;
import java.util.Iterator;

/**
 * Bean与ResultSet对应结果映射类
 * 
 * @author YYDF
 *
 */
public final class BeanMapping {
	private String[] keys;
	private Field[] values;
	private int cursor = 0;

	public BeanMapping(int capacity) {
		// 初始化长度为16
		this.keys = new String[capacity];
		this.values = new Field[capacity];
	}

	public void put(String label, Field field) {
		this.keys[this.cursor] = label;
		this.values[this.cursor] = field;
		cursor++;
	}

	// private void grow(int length) {
	// Entry[] temp = new Entry[maxLength + length];
	// System.arraycopy(this.items, 0, temp, 0, maxLength);
	// this.items = temp;
	// }

	public Iterator<Object[]> iterator() {
		return new Itr();
	}

	private class Itr implements Iterator<Object[]> {
		int step = 0;

		@Override
		public boolean hasNext() {
			return step != BeanMapping.this.cursor;
		}

		@Override
		public Object[] next() {
			final Object[] entry = new Object[2];
			entry[0] = BeanMapping.this.keys[this.step];
			entry[1] = BeanMapping.this.values[this.step];
			this.step++;
			return entry;
		}

	}

}
