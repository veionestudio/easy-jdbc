package cn.coder.jdbc;

public class Simple {
	public static void main(String[] args) {
		SqlSessionFactory.createSessions();
		SqlSession session = SqlSessionFactory.getInstance().getSession();
		SqlTranction tran =null;
		try {
			tran = session.beginTranction();
			Weike w = new Weike();
			w.setTitle("ss"+System.nanoTime());
			session.insert(w);
			
			session.selectOne(Weike.class, "select * from weike where id=?", 2);
			tran.commit();
		} catch (Exception e) {
			tran.rollback(e);
			System.out.println("12");
		}
		
		try {
			tran = session.beginTranction();
			Weike w = new Weike();
			w.setTitle("ss"+System.nanoTime());
			session.insert(w);
			
			session.selectOne(Weike.class, "select * from weike where id=?", 2);
			tran.commit();
		} catch (Exception e) {
			tran.rollback(e);
			System.out.println("12");
		}
		try {
			Weike weike = session.selectOne(Weike.class, "select * from weike where id=?", 2);
			System.out.println(weike.getTitle());
			System.out.println(session.selectOne(Integer.class, "SELECT count(1) from test"));
		} finally {
			
		}
		SqlSessionFactory.destory();
	}
}
